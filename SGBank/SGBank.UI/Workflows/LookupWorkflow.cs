﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGBank.Models;
using SGBanks.BLL;

namespace SGBank.UI.Workflows
{
    public class LookupWorkflow
    {
        private Account _currentAccount;

        public void Execute()
        {
            string input = "";
            int accountNumber = GetAccountNumberFromUser();
            DisplayAccountInformation(accountNumber);

            /*do
            {
                Console.WriteLine();
                Console.WriteLine("1. Deposit");
                Console.WriteLine("2. Withdraw");
                Console.WriteLine("3. Transfer");
                Console.WriteLine();
                Console.WriteLine("Press Q to quit");
                Console.WriteLine();
                Console.Write("Enter Choice: ");
                input = Console.ReadLine();
                
                if (input.ToUpper() != "Q")
                {
                    string choice = input;

                    switch (choice)
                    {

                        case "1":
                            Console.WriteLine("Not implemented yet.");
                            break;
                        case "2":
                            //Withdraw blah = new Withdraw();
                            Withdraw seeIfValid = new Withdraw();
                            seeIfValid.RequestWithdraw(accountNumber);

                            //Console.WriteLine("That was not a valid account number.");
                            Console.WriteLine("Press enter to continue...");
                            Console.ReadLine();
                            break;
                        case "3":
                            Console.WriteLine("Not implemented");
                            break;
                        default:
                            Console.WriteLine("{0} is an invalid entry!", choice);
                            Console.WriteLine("Press enter to continue...");
                            Console.ReadLine();
                            break;
                    }
                }
            } while (input.ToUpper() != "Q");*/
        }

        
        public int GetAccountNumberFromUser()
        {
            do
            {
                Console.Clear();
                Console.Write("Enter an account number: ");
                string input = Console.ReadLine();

                int accountNumber;
                if (int.TryParse(input, out accountNumber))
                {
                    return accountNumber;
                }

                Console.WriteLine("That was not a valid account nunmber...");
                Console.WriteLine("Press enter to continue...");
                Console.ReadLine();

            } while (true);
        }

        public void DisplayAccountInformation(int AccountNumber)
        {
            var ops = new AccountOperations();
            var response = ops.GetAccount(AccountNumber);

            if (response.Success)
            {
                _currentAccount = response.AccountInfo;
                PrintAccountInformation(response.AccountInfo);
                DisplayAccountMenu();
            }
            else
            {
                Console.WriteLine("Error Occurred!!");
                Console.WriteLine(response.Message);
                Console.WriteLine("Move Along...");
            }
        }

        public void PrintAccountInformation(Account AccountInfo)
        {
            Console.Clear();
            Console.WriteLine("Account Information");
            Console.WriteLine("-------------------");
            Console.WriteLine("Account Number: {0}", AccountInfo.AccountNumber);
            Console.WriteLine("Name: {0}, {1}", AccountInfo.LastName, AccountInfo.FirstName);
            Console.WriteLine("AccountBalance: {0:c}", AccountInfo.Balance);
            Console.WriteLine();
            Console.WriteLine("Press enter to continue...");
        }

        public void DisplayAccountMenu()
        {
            string input = "";
            do
            {
                Console.WriteLine("1. Deposit");
                Console.WriteLine("2. Withdraw");
                Console.WriteLine("3. Transfer");
                Console.WriteLine();
                Console.WriteLine("(Q) to Quit");
                Console.WriteLine();
                Console.WriteLine();
                Console.Write("Enter Choice: ");

                input = Console.ReadLine();

                if (input.ToUpper() != "Q")
                {
                    ProcessChoice(input);
                }

            } while (input.ToUpper() != "Q");
        }

        public void ProcessChoice(string Choice)
        {
            switch (Choice)
            {
                case "1":
                    var depositWF = new DepositWorkflow();
                    depositWF.Execute(_currentAccount);
                    PrintAccountInformation(_currentAccount);
                    DisplayAccountMenu();
                    break;
                case "2":
                    var withdrawWF = new WithdrawWorkflow();
                    withdrawWF.Execute(_currentAccount);
                    PrintAccountInformation(_currentAccount);
                    DisplayAccountMenu();
                    break;
                case "3":
                    Console.WriteLine("This feature is not implemented.");
                    Console.WriteLine("Press Enter to continue...");
                    Console.ReadLine();
                    break;
                default:
                    Console.WriteLine("Invalid Entry...");
                    Console.WriteLine("Press Enter to continue...");
                    Console.ReadLine();
                    break;
            }
        }
    }
}
