﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGBank.Data;
using SGBank.Models;

namespace SGBank.UI.Workflows
{
    public class Withdraw
    {
        public decimal LookupBalance(Account AccountInfo)
        {
            decimal balance = AccountInfo.Balance;

            return balance;
        }

        public void RequestWithdraw(int account)
        {
            Console.Write("Enter the amount you want to withdraw: ");
            decimal withdrawAmount = decimal.Parse(Console.ReadLine());
            //TODO Check for negative input.


            //Account Accountinfo = new Account();
            //Accountinfo.AccountNumber = account;
            //Accountinfo.Balance = account;
            var repo = new AccountRepository();
            var currentbalance = repo.GetAccount(account);
            
            if (withdrawAmount > currentbalance.Balance)
            {
                Console.WriteLine("Insufficient funds: Requested {0} but you only have {1}", withdrawAmount, currentbalance.Balance);
            }
            else
            {
                decimal remainingBalance = currentbalance.Balance - withdrawAmount;
                Console.WriteLine("Your remaining balance is: {0}", remainingBalance);
            }
        }
    }
}
